class Warehouse:
    def __init__(self, name):
        self._name = name

    name = property()

    @name.getter
    def name(self):
        """
        Наименование склада
        """
        return self._name

    @name.setter
    def name(self, val):
        self._name = val

    @name.deleter
    def name(self):
        self._name = None
