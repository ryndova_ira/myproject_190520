import telebot

from warehouse import my_warehouse

token = '1184152928:AAFvsnRFoML33KdHGu4xSx_GgQXAZrDueK0'

bot = telebot.TeleBot(token)


@bot.message_handler(commands=['get_name'])
def get_name(message):
    bot.send_message(message.chat.id, my_warehouse.name)


def dict_to_str_beautiful(my_dict):
    return str(my_dict)


@bot.message_handler(commands=['get_goods_list'])
def get_goods_list(message):
    bot.send_message(message.chat.id, dict_to_str_beautiful({"Bread": 4, "Milk": 9}))


if __name__ == '__main__':
    print('Starting bot...')
    bot.polling(none_stop=True, interval=0)
